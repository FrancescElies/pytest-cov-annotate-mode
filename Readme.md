# PYTEST-COV-ANNOTATE-MODE

A mode for highlighting coverage in `.py,cover` files generated with `py.test`

To get such files run your tests with the flag `--cov-report annotate:cov_annotate`

For example:

  py.test --cov-report annotate:cov_annotate --cov=myproject tests/

The previous command should generate `.py,cover` files next to your python `.py`
files.

This mode adds line highlighting for these files, it will highlight lines with
green if the test runner executed that part of the code, otherwise it will show
the code with a red face.

## Installation

TODO

## Usage

Just open a `.py,cover` file and see the lines that have been covered.

